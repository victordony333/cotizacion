
const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const misRutas = require("./router/index.js");
const path = require("path");

const app = express();

app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({extended:true}));
app.use(misRutas);



//DECLARAR UN ARRAYS DE OBJETOS


app.get('/tabla',(req,res)=>{

    const params = {
        numero:req.query.numero
    }
    res.render('tabla',params);
})

app.post('/tabla',(req,res)=>{

    const params = {
        numero:req.body.numero
    }
    res.render('tabla',params);
})

//Cotizacion

app.get('/cotizacion',(req,res)=>{

    const params = {
        valor:req.query.valor,
        pinicial:req.query.pinicial,
        plazo:req.query.plazo,
        pmensual:req.query.pmensual
    }
    res.render('cotizacion',params);
})

app.post('/cotizacion',(req,res)=>{

    const params = {
        valor:req.body.valor,
        pinicial:req.body.pinicial,
        plazo:req.body.plazo,
        pmensual:req.body.pmensual
    }
    res.render('cotizacion',params);
})

// La pagina del error va al final de get/post
app.use((req,res,next)=>{

    res.status(404).sendFile(__dirname
        + '/public/error.html');

})


const puerto = 3000;
app.listen(puerto,()=>{

    console.log("Iniciando puerto");

})
