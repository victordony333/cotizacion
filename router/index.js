const bodyparser = require("body-parser");
const express = require("express");
const router = express.Router();

module.exports = router;

//DECLARAR UN ARRAYS DE OBJETOS

let datos = [{

    matricula:"2020030667",
    nombre: "Victor Donnovan Romero Sandoval",
    sexo: "M",
    materias: ["Ingles", "Base de datos", "Tecnologia I"]

},
{

    matricula:"2020030220",
    nombre: "Angel Manuel Hernandez Velasquez",
    sexo: "M",
    materias: ["Ingles", "Base de datos", "Redes"]

},
{

    matricula:"2020030720",
    nombre: "Jesus Alejandro Peinado Avila",
    sexo: "M",
    materias: ["Ingles", "Redes", "Fisica"]

}

]

router.get("/",(req,res)=>{

    res.render('index', {titulo: "Mi primer pagina en Embedded JavaScript",
                        nombre: "Victor Donnovan Romero Sandoval",
                        grupo: "8-3",
                        listado:datos})

})


router.get('/tabla',(req,res)=>{

    const params = {
        numero:req.query.numero
    }
    res.render('tabla',params);
})

router.post('/tabla',(req,res)=>{

    const params = {
        numero:req.body.numero
    }
    res.render('tabla',params);
})

//Cotizacion

router.get('/cotizacion',(req,res)=>{

    const params = {
        valor:req.query.valor,
        pinicial:req.query.pinicial,
        plazo:req.query.plazo,
        pmensual:req.query.pmensual
    }
    res.render('cotizacion',params);
})

router.post('/cotizacion',(req,res)=>{

    const params = {
        valor:req.body.valor,
        pinicial:req.body.pinicial,
        plazo:req.body.plazo,
        pmensual:req.body.pmensual
    }
    res.render('cotizacion',params);
})
